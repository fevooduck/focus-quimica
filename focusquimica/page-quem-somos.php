<?php get_header(); 

/* Template Name: Institucional */

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1><?php the_title(); ?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas">
	<div class="container">
        <div class="row margin-conteudo">
        	<div class="col-md-8 text-justify">
            	<?php the_content('Read the rest of this entry &raquo;'); ?>
            </div>
            <?php endwhile; endif; ?>
            <div class="col-md-4">
				<div class="box-quem-somos">
                  <h2 style="margin-top:0;">Missão</h2>
                  <?php $texto = get_post_meta( $post->ID, 'missao', true ); echo $texto; ?>             
                  <h2>Visão</h2>
                  <?php $texto = get_post_meta( $post->ID, 'visao', true ); echo $texto; ?>
                  <h2>Valores</h2>
                  <?php $texto = get_post_meta( $post->ID, 'valores', true ); echo $texto; ?>             
              </div>
            </div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
