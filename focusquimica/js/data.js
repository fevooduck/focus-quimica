/*
    * The marker data will need to match this structre to work
    * To filter upon a specific key it needs to be hooked up in the filter section
        of the myMap object

    This data was generated from http://www.json-generator.com/
    

    Here is the source for generating the JSON
    
    // start
    [
        '{{repeat(150)}}',
        {
            id: '{{index}}',
            name: '{{firstName}} {{surname}}',
            age: '{{numeric(20,85)}}',
            followers: '{{numeric(8,80)}}',
            occupation: '{{lorem(2)}}',
            from: 'Michigan',
            college: function(idx) {
                var choices = ['MSU', 'MTU', 'UM', 'CMU', 'FSU'];
                return choices[idx.numeric(0, choices.length - 1)];
            },
            lat: '{{numeric(31.969639, 43.849384)}}',
            lng: '{{numeric(-120.629883, -77.387695)}}'
        }
    ]
    // finish

*/
var personData = [
    {
        "id": 0,
        "name": "SANTANA",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Rua Voluntários da Pátria, 2023",
        "bairro": "Santana",
        "cep": "02011-400",
        "telefone": "(11) 3459-4517",
        "horario": "Segunda a Sábado: das 09h às 20h<br>Domingo e Feriados: 09h às 14h",
        "lat": -23.5024305,
        "lng": -46.626519
    },
    {
        "id": 1,
        "name": "MOOCA",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Rua Da Mooca, 2536",
        "bairro": "Mooca",
        "cep": "03104-002",
        "telefone": "(11) 2528-5576",
        "horario": "Segunda a Sábado: das 09h às 19h30<br>Domingo e Feriados: 10h às 15h",
        "lat": -23.5555938,
        "lng": -46.6005331
    },
    {
        "id": 2,
        "name": "PENHA",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Av. Penha de França, 395",
        "bairro": "Penha",
        "cep": "03606-010 ",
        "telefone": "(11) 3805-3267",
        "horario": "Segunda a Sábado: das 09h às 20h<br>Domingo e Feriados: 09h às 15h",
        "lat": -23.5230343,
        "lng": -46.5479466
    },
    {
        "id": 3,
        "name": "SÃO MATHEUS",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Av. Mateo Bei, 3376",
        "bairro": "São Matheus",
        "cep": "03949-013",
        "telefone": "(11) 2253-1343",
        "horario": "Segunda a Sábado: das 09h às 20h<br>Domingo e Feriados: 09h às 14h",
        "lat": -23.6101554,
        "lng": -46.4770483
    },
    {
        "id": 4,
        "name": "VILA SABRINA",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Av. Milton da Rocha, 82",
        "bairro": "Vila Sabrina",
        "cep": "02138-010 ",
        "telefone": "(11) 2539-6094",
        "horario": "Segunda a Sábado: das 09h às 20h<br>Domingo e Feriados: 10h às 14h",
        "lat": -23.4907457,
        "lng": -46.5742274
    },
    {
        "id": 5,
        "name": "AUGUSTA",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Rua Augusta, 2496",
        "bairro": "Cerqueira César",
        "cep": "01412-100",
        "telefone": "(11) 3061-9636",
        "horario": "Segunda a Sábado: das 10h às 20h<br>Domingo e Feriados: 11h às 17h",
        "lat": -23.5629753,
        "lng": -46.6655952
    },
    {
        "id": 6,
        "name": "GALERIA OSASCO",
        "from": "SP",
        "college": "Osasco",
        "endereco": "Rua Dona Primitiva Vianco, 100 Lj 32",
        "bairro": "Centro",
        "cep": "06016-000",
        "telefone": "(11) 3681-7536",
        "horario": "Segunda a Sábado: das 09h às 20h",
        "lat": -23.5290215,
        "lng": -46.7760271
    },
    {
        "id": 7,
        "name": "SÃO CARLOS",
        "from": "SP",
        "college": "São Carlos",
        "endereco": "Rua General Osório, 821",
        "bairro": "Centro",
        "cep": "13560-640",
        "telefone": "(16) 3413-7495",
        "horario": "Segunda a Sábado: das 09h às 18h",
        "lat": -22.0216915,
        "lng": -47.8838285
    },
    {
        "id": 8,
        "name": "CASCÁVEL",
        "from": "PR",
        "college": "Cascável",
        "endereco": "Rua 7 de Setembro, 2958 Sala 3",
        "bairro": "Centro",
        "cep": "85801-140",
        "telefone": "(45) 3038-2958",
        "horario": "Segunda a Sábado: das 09h às 18h30",
        "lat": -24.9564432,
        "lng": -53.4604551
    },
    {
        "id": 9,
        "name": "TATUAPÉ",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Rua Tuiuti, 1941",
        "bairro": "Tatuapé",
        "cep": "03307-005",
        "telefone": "(11) 4561-0503",
        "horario": "Segunda a Sábado: das 09h às 20h",
        "lat": -23.5425446,
        "lng": -46.5749369
    },
    {
        "id": 10,
        "name": "ITAQUAQUECETUBA",
        "from": "SP",
        "college": "Itaquaquecetuba",
        "endereco": "Rua Capitão José Leite, 37",
        "bairro": "Centro",
        "cep": " 08570-030",
        "telefone": "(11) 4648-4741",
        "horario": "Segunda a Sábado: das 09h às 20h<br>Domingo e Feriados: 10h às 14h",
        "lat": -23.4757541,
        "lng": -46.352281
    },
    {
        "id": 11,
        "name": "ITATIBA",
        "from": "SP",
        "college": "Itatiba",
        "endereco": "Rua Rui Barbosa, 251",
        "bairro": "Centro",
        "cep": "13250-280",
        "telefone": "(11) 4487-1301",
        "horario": "Segunda a Sábado: das 09h às 19h",
        "lat": -23.0040627,
        "lng": -46.8431359
    },
    {
        "id": 12,
        "name": "ITAIM PAULISTA",
        "age": 52,
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Av. Marechal Tito, 4490-A",
        "bairro": "Santana",
        "cep": "08115-000",
        "telefone": "(11) 2025-6698",
        "horario": "Segunda a Sábado: das 09h às 20h<br>Domingo e Feriados: 09h às 15h",
        "lat": -23.4969364,
        "lng": -46.4010347
    },
    {
        "id": 13,
        "name": "IPIRANGA",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Rua Silva Bueno, 2253",
        "bairro": "Ipiranga",
        "cep": "04208-053",
        "telefone": "(11) 3562-8992",
        "horario": "Segunda a Sábado: das 09h às 20h<br>Domingo e Feriados: 09h às 14h",
        "lat": -23.5984902,
        "lng": -46.6004521
    },
    {
        "id": 14,
        "name": "RIBEIRÃO PRETO",
        "from": "SP",
        "college": "Ribeirão Preto",
        "endereco": "Rua Tibiriçá, 539",
        "bairro": "Centro",
        "cep": "14010-090",
        "telefone": "(16) 3236-3878",
        "horario": "Segunda a Sábado: das 09h às 18h30<br>Domingo e Feriados: 09h às 15h",
        "lat": -21.1763939,
        "lng": -47.8097939
    },
    {
        "id": 15,
        "name": "CENTRO CASCÁVEL",
        "from": "PR",
        "college": "Cascável",
        "endereco": "Av. Brasil, 5924",
        "bairro": "Centro",
        "cep": "85801-000",
        "telefone": "(45) 3039-5924",
        "horario": "Segunda a Sábado: das 09h às 18h30",
        "lat": -24.9553586,
        "lng": -53.4549002
    },
    {
        "id": 16,
        "name": "GUAIANAZES",
        "from": "SP",
        "college": "São Paulo",
        "endereco": "Rua Salvador Gianetti, 1020",
        "bairro": "Guaianazes",
        "cep": "08410-000",
        "telefone": "(11) 2016-8093",
        "horario": "Segunda a Sábado: das 09h às 20h<br>Domingo e Feriados: 09h às 14h",
        "lat": -23.5436209,
        "lng": -46.4124476
    },
    {
        "id": 17,
        "name": "BOULEVARD NAÇÕES",
        "from": "SP",
        "college": "Bauru",
        "endereco": "Rua General Marcondes Salgado Quadra 11-39 Lj363",
        "bairro": "Chácara das Flores",
        "cep": "17013-113",
        "telefone": "(14) 3233-7484",
        "horario": "Segunda a Sábado: das 10h às 22h<br>Domingo e Feriados: 14h às 20h",
        "lat": -22.3169646,
        "lng": -49.0649836
    },
    {
        "id": 18,
        "name": "POÇOS DE CALDAS",
        "from": "MG",
        "college": "Poços de Caldas",
        "endereco": "Rua Assis Figueiredo, 1194",
        "bairro": "Centro",
        "cep": "37701-000",
        "telefone": "(35) 3714-8785",
        "horario": "Segunda a Sábado: das 09h às 19h<br>Domingo e Feriados: 10h às 16h",
        "lat": -21.7889919,
        "lng": -46.5663686
    },
    {
        "id": 19,
        "name": "TOLEDO",
        "from": "PR",
        "college": "Toledo",
        "endereco": "Rua Barão do Rio Branco, 1060",
        "bairro": "Centro",
        "cep": "85901-180",
        "telefone": "(45) 3054-6960",
        "horario": "-",
        "lat": -24.7193097,
        "lng": -53.7385011
    }
]
