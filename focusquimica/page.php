<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1><?php the_title(); ?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas">
	<div class="container">
        <div class="row margin-conteudo">
        	<div class="col-md-6">
            	<?php the_content('Read the rest of this entry &raquo;'); ?>
            </div>
            <?php endwhile; endif; ?>
            <div class="col-md-6">
              <div id="carousel-slide-sobre" class="carousel slide slide-sobre" data-ride="carousel">
              
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                <?php $cont=0;?>
                <?php $wp_query = new WP_Query(array( 
                      'post_type' 		=> 'slideSobre',
                      'orderby'           => 'menu_order',
                      'order' 		    => 'ASC',
                      'posts_per_page'    => -1
              ));
              while ( have_posts() ) : the_post();
              $cont++ ?>
              <?php
                  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
                  $titulo = get_the_title();
              
              
               ?>
                  <div class="item <?php if($cont==1){?>active<?php } ?>">
                      <img src="<?php echo $html[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive">
                  </div>
              <?php
              
              
              wp_reset_postdata();
              
              endwhile; 
              
              $totalcont = $cont;
              
              $numCont = $totalcont - 1;
              ?>
              </div>  
                  <?php if($totalcont >= 2){ ?>
               <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#carousel-slide-sobre" data-slide-to="0" class="active"></li>
                  <?php for ($valor = 1; $valor <= $numCont; $valor++){
                  ?>
                  <li data-target="#carousel-slide-sobre" data-slide-to="<?php echo $valor;?>"></li>
                  <?php } ?>
                </ol>
                  <?php } ?>
                
              </div>
              <script type="text/javascript">
			  	$('.slide-sobre').carousel({
				  interval: 2000
				})
			  </script>
            </div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
