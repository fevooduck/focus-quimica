<?php get_header(); 

/* Template Name: Representadas */

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1><?php the_title(); ?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
	</div>
</section>
<?php endwhile; endif; ?>
<section class="internas-representadas">
	<div class="container">
        <div class="row margin-conteudo">
    <?php $wp_query = new WP_Query(array( 
          'post_type' 		=> 'representadas',
          'orderby'           => 'menu_order',
          'order' 		    => 'ASC',
          'posts_per_page'    => -1
  ));
  while ( have_posts() ) : the_post();

      $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
      $titulo = get_the_title();
      $frase = get_post_meta( $post->ID, '_representadas_frase', true );
      $apoio = get_post_meta( $post->ID, '_representadas_link', true );
      $link = get_page_template_slug( $post->ID );
  
  
   ?>
		<div class="col-md-4">
       	<div class="box-representada">
        <a  class="logo-peq" href="<?php echo home_url(); ?>/representadas/<?php echo $link; ?>">
        <img src="<?php echo $html[0]; ?>" alt="<?php echo $titulo; ?>" class="img-responsive">
        </a>
        <h3 class="frase-representada-interna text-center"><?php echo $frase; ?></h3>
		<div class="box-representada-texto">
       		<p class="text-center"><?php echo $apoio; ?></p>
		</div>
        <p class="text-center"><a href="<?php echo home_url(); ?>/representadas/<?php echo the_slug(); ?>" class="btn btn-focus">Lista de Produtos</a></p>
			</div>
        </div>
  <?php
  wp_reset_postdata();
  
  endwhile; 
  ?>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
