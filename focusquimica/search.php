<?php get_header();?>
<?php if (have_posts()) : ?>
<section class="cab-page">
  <div class="container">
      <div class="page-header">
          <div class="row">
              <div class="col-md-12">
                  <h1><?php $mySearch =& new WP_Query("s=$s & showposts=-1 & order=ASC & orderby=title"); $num = $mySearch->post_count; echo $num.' resultados para sua busca por: '; the_search_query();?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
  </div>
</section>
<section class="internas">
  <div class="container">
    <div class="margin-conteudo">
<?php $cont = 0; while (have_posts()) : the_post(); $cont++; 

?>
          <p class="text-left"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
          </p>
<?php endwhile;?>
    </div>
    <div class="text-right"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>

<?php else : ?>
<section class="cab-page">
  <div class="container">
      <div class="page-header">
          <div class="row">
              <div class="col-md-12">
                  <h1>Sua pesquisa por <?php the_search_query();?> não encontrou nenhum resultado.</h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
  </div>
</section>
<section class="internas">
  <div class="container">
    <div class="row margin-conteudo">
      <div class="col-md-8 text-justify">
<p>Sugestões:</p>
<ul>
   <li>  Certifique-se de que todas as palavras estão escritas corretamente..</li>
   <li>  Tente palavras-chave diferentes..</li>
   <li>  Tente palavras-chave mais gerais.</li>
</ul>
      </div>
    </div>
<?php endif; ?>
    </div>
</section>
  <?php get_footer(); ?>