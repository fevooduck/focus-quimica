<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
    	  <section class="feature" data-stellar-background-ratio="0.5"></section>
   	  <section class="sub-header">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-2">
        	<div class="retangulo-page-header"></div>
            	</div>
                <div class="col-md-6">
            <div class="page-header">
            	<h1 class="titulo-pagina">
                	Erro 404<br>
                    <small>Página não encontrada</small>
                </h1>
                    <small><?php $texto = get_post_meta( $post->ID, 'texto', true ); echo $texto; ?></small>
                <div class="barra-page-header"></div>
            </div>
            </div>
            <div class="col-md-4">
            	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            </div>
            </div>
        </div>
      </section>
      <section class="wrapper">
      	<div class="container">
        	<div class="row">
            	<div class="col-md-6 col-md-offset-2">
        <p>Pedimos desculpa, mas a página que acessou não está mais disponível! Poderá ter sido removida ou alterada.</p>
        <p>Já verificou na barra de endereço do seu browser de internet se o URL está correto?.</p>
        <p>Se desejar, você pode ir para a nossa <a href="http://www.easysxx.com">Home</a></p>
        <p>Se desejar reportar esse erro, <a href="mailto:lbmf@lbmf.com.br">contate-nos</a>, para que o possamos resolver. Obrigado!</p>            
                </div>
            	<div class="col-md-3 col-md-offset-1">
                    <img src="<?php bloginfo('template_directory'); ?>/img/img-noticias-home-marcacao.jpg" alt="" class="img-responsive">
                </div>
            </div>
        </div>
      </section>
<?php endwhile; endif; ?>
  <?php get_footer(); ?>