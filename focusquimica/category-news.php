<?php get_header();?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1><?php if (have_posts()) : ?>
					  <?php /* Se é um arquivo de categoria */ if (is_category()) { ?>
							<?php single_cat_title(); }?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas">
	<div class="container">
        <div class="row margin-conteudo">
		   <?php 
		   $cont = 0;
		   while (have_posts()) : the_post(); $cont++;?>
           <div class="col-md-3">
           	<article class="box-sugestao">
           		<img src="<?php bloginfo('template_directory'); ?>/img/icone-news-interna.png" class="img-responsive icone-sugestao">
	            <p class="text-center data-interna"><?php the_time('d/m/Y') ?></p>
	            <h2 class="titulo-sugestao"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			</article>
           </div>
            <?php
			if($cont == 4){
	  echo '</div>
	  <div class="row">';
				$cont = 0;
  };
                    endwhile; endif; ?>

		</div>
		<div class="text-right"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>
    </div>
</section>
  <?php get_footer(); ?>