<footer class="footer">
	<div class="container">
        <div class="row">
        	<div class="col-md-3">
            	<img src="<?php bloginfo('template_directory'); ?>/img/logo-focus-quimica.png" class="img-responsive" alt="Focus Química">
            </div>
        	<div class="col-md-4">
            	<p class="texto-empresa">A Focus Química é uma empresa importadora e distribuidora de Especialidades para o mercado Cosmético.</p>
            </div>
        	<div class="col-md-2">
            	<img src="<?php bloginfo('template_directory'); ?>/img/logo-abihpec.png" class="img-responsive" alt="Focus Química Fornecedor Qualificado 2016 - Abihpec">
            </div>
            <div class="col-md-3">
            	<p>Rua Gaspar Rodrigues, 530</p>
            	<p>CEP 03372-000 São Paulo, SP—Brasil</p>
            	<p>Tel.: +55 (11) 2676-3111</p>
            	<p>Email: <a href="mailto:focusquimica@focusquimica.com">focusquimica@focusquimica.com</a></p>
            </div>
        </div>
    </div>
</footer>
<section class="copyright">
	<div class="container">
    	<p>© 2016 Todos os Direitos Reservados - Focus química</p>
    </div>
</section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery.mobile.custom.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
		  $('#tabs li a').click(function(e) {
		
			$('#tabs li, #content .current').removeClass('current').removeClass('fadeInLeft');
			$(this).parent().addClass('current');
			var currentTab = $(this).attr('href');
			$(currentTab).addClass('current fadeInLeft');
			e.preventDefault();
		
		  });
		  var owl = $('.owl-carousel');
			owl.owlCarousel({
				items:	4,
				loop:true,
				nav: true,
				margin:15,
				autoplay:true,
				autoplayTimeout:8000,
				autoplayHoverPause:true,
				responsiveClass:true,
				responsive:{
					0:{
						items:2,
						nav:true
					},
					600:{
						items:3,
						nav:false
					},
					1000:{
						items:4,
						nav:true,
						loop:false
					}
				}
			});
		});
	</script>
    <?php wp_footer(); ?>
  </body>
</html>