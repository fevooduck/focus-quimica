<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '::', true, 'right' ); ?> <?php bloginfo('name'); ?></title>

    <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/style.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/owl.carousel.css" rel="stylesheet">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php wp_head(); ?>
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27417904-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Google Analytics -->
    
  </head>
  <body>
<header class="header">
	<div class="container">
    	<div class="row">
        	<div class="col-md-3 col-sm-3">
            	<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo-focus10anos.png" class="img-responsive logo-topo hidden-xs" alt="<?php bloginfo('name'); ?>"></a>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 text-right">
				<p>Tel.: +55 (11) 2676-3111</p>
				<p>Email: <a href="mailto:focusquimica@focusquimica.com">focusquimica@focusquimica.com</a></p>
            	<form class="navbar-form navbar-right" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Buscar"  name="s">
        </div>
        <button type="submit" class="btn btn-focus"><i class="glyphicon glyphicon-search"></i></button>
      </form>
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-default navbar-principal">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-principal" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
            	<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/logo-focus-quimica.png" class="img-responsive logo-topo visible-xs" alt="<?php bloginfo('name'); ?>"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="menu-principal">

        <?php wp_nav_menu(array(
        'container'       => false,
        'items_wrap'      => '<ul id="%1$s" class="nav navbar-nav menu-principal">%3$s</ul>',
        'theme_location'	=> 'principal',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker()
        ));
        ?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

