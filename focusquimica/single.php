<?php get_header();?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1><?php the_title(); ?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas">
	<div class="container">
        <div class="row margin-conteudo">
        	<div class="col-md-8 text-justify">
				<p class="text-right"><?php the_time('d/m/Y') ?></p>
           		<hr>
            	<?php the_content('Read the rest of this entry &raquo;'); ?>
            </div>
            <?php endwhile; endif; ?>
            <div class="col-md-4">
				<h3 class="titulo-sidebar">Últimas Notícias</h3>
				<div class="barra-sidebar"></div>
           	<?php $cont=0;
    $wp_query = new WP_Query(array( 
          'post_type' 		=> 'post',
          'orderby'           => 'date',
          'order' 		    => 'DESC',
          'posts_per_page'    => 5,
		  'category_name'		=> 'news'
  ));
  while ( have_posts() ) : the_post();
  $cont++; 
  $titulo = get_the_title();
  $link = get_permalink();
  
  
   ?>
      <p><span class="numero-lateral"><?php echo $cont; ?>.</span><a class="link-lateral" href="<?php echo $link; ?>"><?php echo $titulo; ?></a></p>
  <?php
  wp_reset_postdata();
  
  endwhile; 
  
  ?>
<hr>
				<h3 class="titulo-sidebar">Últimas Sugestões</h3>
				<div class="barra-sidebar"></div>
           	<?php $cont=0;
    $wp_query = new WP_Query(array( 
          'post_type' 		=> 'post',
          'orderby'           => 'date',
          'order' 		    => 'DESC',
          'posts_per_page'    => 5,
		  'category_name'		=> 'sugestoes-de-formulas'
  ));
  while ( have_posts() ) : the_post();
  $cont++; 
  $titulo = get_the_title();
  $link = get_permalink();
  
  
   ?>
      <p><span class="numero-lateral"><?php echo $cont; ?>.</span><a class="link-lateral" href="<?php echo $link; ?>"><?php echo $titulo; ?></a></p>
  <?php
  wp_reset_postdata();
  
  endwhile; 
  
  ?>

            </div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
