<?php

add_action( 'init', 'representadas_post_type' );
function representadas_post_type() {
	register_post_type( 'representadas',
		array(
			'labels' => array(
				'name' 				=> 'Representadas',
				'singular_name' 	=> 'Representada',
				'menu_name'         => 'Representadas',
				'all_items'         => 'Todos as representadas',
				'view_item'         => 'Ver Representada',
				'add_new_item'      => 'Adicionar nova Representada',
				'add_new'           => 'Adicionar Representada',
				'edit_item'         => 'Alterar Representada',
				'update_item'       => 'Atualizar Representada',
				'search_items'      => 'Pesquisar Representada',
				'not_found'         => 'Nenhuma Representada Encontrada',
				'not_found_in_trash'=> 'Nenhuma Representada Encontrada na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => true,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-nametag',
    	'menu_position' => 5,
		'supports'            => array( 'title', 'thumbnail', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'empresas-representadas'),
    	)
	);
	
	flush_rewrite_rules();
}

function ep_representadasposts_metaboxes() {
	add_meta_box( 'ept_representada_add', 'Informações Adicionais', 'ept_representada_add', 'representadas', 'normal', 'default', array('id'=>'_add') );
}
add_action( 'admin_init', 'ep_representadasposts_metaboxes' );
 
function ept_representada_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_representadasposts_nonce' );
    // The metabox HTML
	echo '<p>Frase de Apoio:</p>';
    $representadas_frase = get_post_meta( $post->ID, '_representadas_frase', true );
    echo '<input type="text" name="_representadas_frase" value="' . $representadas_frase  . '"  style="width:99%"/>';
	echo '<p>Texto de Apoio:</p>';
    $representadas_link = get_post_meta( $post->ID, '_representadas_link', true );
    echo '<textarea name="_representadas_link"  style="width:99%">' . $representadas_link  . '</textarea>';
	echo '<p>País:</p>';
    $representadas_pais = get_post_meta( $post->ID, '_representadas_pais', true );
    echo '<input type="text" name="_representadas_pais" value="' . $representadas_pais  . '"  style="width:99%"/>';
	echo '<p>Site:</p>';
    $representadas_site = get_post_meta( $post->ID, '_representadas_site', true );
    echo '<input type="text" name="_representadas_site" value="' . $representadas_site  . '"  style="width:99%"/>';
	echo '<p>Slug Marca:</p>';
    $representadas_marca = get_post_meta( $post->ID, '_representadas_marca', true );
    echo '<input type="text" name="_representadas_marca" value="' . $representadas_marca  . '"  style="width:99%"/>';
}

// Save the Metabox Data
function ep_representadasposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_representadasposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_representadasposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though
 	$representadas_meta_save['_representadas_frase'] = $_POST['_representadas_frase'];
 	$representadas_meta_save['_representadas_link'] = $_POST['_representadas_link'];
 	$representadas_meta_save['_representadas_pais'] = $_POST['_representadas_pais'];
 	$representadas_meta_save['_representadas_site'] = $_POST['_representadas_site'];
 	$representadas_meta_save['_representadas_marca'] = $_POST['_representadas_marca'];
 	
    // Add values of $events_meta as custom fields
    foreach ( $representadas_meta_save as $key => $value ) { // Cycle through the $events_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_representadasposts_save_meta', 1, 2 );

?>