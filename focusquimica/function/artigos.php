<?php

add_action( 'init', 'artigos_post_type' );
function artigos_post_type() {
	register_post_type( 'artigos',
		array(
			'labels' => array(
				'name' 				=> 'Artigos',
				'singular_name' 	=> 'Artigo',
				'menu_name'         => 'Artigos',
				'all_items'         => 'Todos os artigos',
				'view_item'         => 'Ver Artigo',
				'add_new_item'      => 'Adicionar novo artigo',
				'add_new'           => 'Adicionar artigo',
				'edit_item'         => 'Alterar artigo',
				'update_item'       => 'Atualizar artigo',
				'search_items'      => 'Pesquisar artigo',
				'not_found'         => 'Nenhum Artigo Encontrado',
				'not_found_in_trash'=> 'Nenhum Artigo Encontrado na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-format-aside',
    	'menu_position' => 5,
		'supports'            => array( 'title', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'artigos'),
    	)
	);
	
	flush_rewrite_rules();
}

function ep_artigosposts_metaboxes() {
	add_meta_box( 'ept_artigos_add', 'Arquivo PDF', 'ept_artigos_add', 'artigos', 'normal', 'default', array('id'=>'_add') );
}
add_action( 'admin_init', 'ep_artigosposts_metaboxes' );
 
function ept_artigos_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_artigosposts_nonce' );
    // The metabox HTML
	echo '<p>Link Documento:</p>';
    $artigos_link = get_post_meta( $post->ID, '_artigos_link', true );
    echo '<input type="text" name="_artigos_link" value="' . $artigos_link  . '"  style="width:99%"/>';
}

// Save the Metabox Data
function ep_artigosposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_artigosposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_artigosposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though
 	$artigos_meta_save['_artigos_link'] = $_POST['_artigos_link'];
 	
    // Add values of $events_meta as custom fields
    foreach ( $artigos_meta_save as $key => $value ) { // Cycle through the $events_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_artigosposts_save_meta', 1, 2 );

?>