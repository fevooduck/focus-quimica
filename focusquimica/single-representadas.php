<?php get_header(); 

/* Template Name: Representadas */

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-7">
                	<h1><?php the_title(); ?></h1>
                    <span class="seta-cab"></span>
                </div>
                <div class="col-md-5">
                	<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas-representadas margin-conteudo">
	<div class="container">
   		<?php the_content(); ?>
    </div>
</section>
<?php endwhile; endif; ?>
  <?php get_footer(); ?>
  
