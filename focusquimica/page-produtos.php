<?php get_header(); 

/* Template Name: Produtos */

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); 

	  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );

?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1><?php the_title(); ?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas">
	<div class="container">
        <div class="row margin-conteudo">
        	<div class="col-md-7 text-justify">
		        <img src="<?php echo $html[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive cab-produtos">
	        	<?php the_content('Read the rest of this entry &raquo;'); ?>
            </div>
            <?php endwhile; endif; ?>
        	<div class="col-md-4 col-md-offset-1">
            	<div class="box-representada">
      		        
 <?php $categoria = get_post_meta($post->ID,'marca', true); ?>
                            	   <?php 
					   $query = new WP_Query( array( 'post_type' => 'representadas', 'orderby'=> 'date', 'order' => 'ASC', 'meta_key' => '_representadas_marca', 'meta_value' => $categoria));
    // run the loop based on the query
    if ( $query->have_posts() ) { 
	while ($query->have_posts()) : $query->the_post();
	
	global $post;
		
	  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
      $frase = get_post_meta( $post->ID, '_representadas_frase', true );
      $pais = get_post_meta( $post->ID, '_representadas_pais', true );;
      $site = get_post_meta( $post->ID, '_representadas_site', true );

	?>
      		        
      		        
       		        <img src="<?php echo $html[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive">
            		<h3 class="frase-representada-interna"><?php echo $frase; ?></h3>
					<p>País: <?php echo $pais; ?></p>
					<p>Site: <a href="http://<?php echo $site; ?>" alt="<?php the_title(); ?>" target="_blank"><?php echo $site; ?></a></p>
					  <?php endwhile;
        //$myvariable = ob_get_clean();
        //return $myvariable;
        wp_reset_postdata(); }?>
				</div>
            	<div class="box-representada">
					<h3 class="titulo-sidebar">Outros Produtos</h3>
				        <div class="barra-sidebar"></div>
					        <?php wp_nav_menu(array(
							'container'       => false,
							'items_wrap'      => '<ul id="%1$s" class="nav nav-sidebar nav-stacked">%3$s</ul>',
							'theme_location'	=> $categoria,
									'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
									'walker'            => new wp_bootstrap_navwalker()
							));
							?>

				</div>
            </div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
