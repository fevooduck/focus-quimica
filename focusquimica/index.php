	<?php get_header();?>
<section class="slider">
    <div class="container">
        <div class="row">
            <div class="col-md-9 box-slide">
                <div id="carousel-slide-home" class="carousel slide" data-ride="carousel">
                
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                    <?php $cont=0;?>
                    <?php $wp_query = new WP_Query(array( 
                        'post_type' 		=> 'slide',
                        'orderby'           => 'menu_order',
                        'order' 		    => 'ASC',
                        'posts_per_page'    => 5
                ));
                while ( have_posts() ) : the_post();
                $cont++ ?>
                <?php
                    $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
                    $titulo = get_the_title();
                    $link = get_post_meta( $post->ID, '_slide_link', true );
                
                
                ?>
                    <div class="item <?php if($cont==1){?>active<?php } ?>">
                        <a href="<?php echo $link; ?>"><img src="<?php echo $html[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive"></a>
                    </div>
                <?php
                
                
                wp_reset_postdata();
                
                endwhile; 
                
                $totalcont = $cont;
                
                $numCont = $totalcont - 1;
                ?>
                </div>  
                    <?php if($totalcont >= 2){ ?>
                <!-- Indicators -->
                    <ol class="carousel-indicators">
                    <li data-target="#carousel-slide-home" data-slide-to="0" class="active"></li>
                    <?php for ($valor = 1; $valor <= $numCont; $valor++){
                    ?>
                    <li data-target="#carousel-slide-home" data-slide-to="<?php echo $valor;?>"></li>
                    <?php } ?>
                    </ol>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-slide-home" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-slide-home" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                    <?php } ?>
                    
                </div>            
            </div>
            <div class="col-md-3 noticias">
            <h3 class="titulo-branco">FOCUS NEWS</h3>
        <div class="barra-titulo-branco"></div>
                <?php 
    $wp_query = new WP_Query(array( 
          'post_type' 		=> 'post',
          'orderby'           => 'date',
          'order' 		    => 'DESC',
          'posts_per_page'    => 4,
		  'category_name'		=> 'news'
  ));
  while ( have_posts() ) : the_post();
  $titulo = get_the_title();
  $link = get_the_permalink();
  
  
   ?>
				<p class="data-home"><?php the_time('d/m/Y') ?></p>
           		<p class="artigo-home"><a class="link-artigo" href="<?php echo $link; ?>" target="_blank"><?php echo $titulo; ?></a></p>
  <?php
  wp_reset_postdata();
  endwhile; 
  ?>
            </div>
        </div>
    </div>
</section>
<section class="empresas-representadas">
	<div class="container">
    	<h3 class="titulo-cinza">EMPRESAS REPRESENTADAS</h3>
        <div class="barra-titulo-verde"></div>
        <div class="owl-carousel">
    <?php $wp_query = new WP_Query(array( 
          'post_type' 		=> 'representadas',
          'orderby'           => 'menu_order',
          'order' 		    => 'ASC',
          'posts_per_page'    => -1
  ));
  while ( have_posts() ) : the_post();

      $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
      $titulo = get_the_title();
      $frase = get_post_meta( $post->ID, '_representadas_frase', true );
      $link = get_the_permalink();
  
  
   ?>
		<div>
        <a class="logo-peq" href="<?php echo home_url(); ?>/representadas/<?php the_slug(); ?>">
        <img src="<?php echo $html[0]; ?>" alt="<?php echo $titulo; ?>" class="img-responsive">
        </a>
        <h4 class="frase-representada text-center"><?php echo $frase; ?></h4>
        <p class="text-center"><a href="<?php echo home_url(); ?>/representadas/<?php echo the_slug(); ?>" class="btn btn-focus">Lista de Produtos</a></p>
        </div>
  <?php
  wp_reset_postdata();
  
  endwhile; 
  ?>
        </div>
    </div>
</section>
<section class="artigos-tecnicos">
	<div class="container">
    	<h3 class="titulo-branco">ARTIGOS TÉCNICOS</h3>
        <div class="barra-titulo-branco"></div>
		<div class="row">
			<div class="col-md-6">
	<?php $cont=0;
    $wp_query = new WP_Query(array( 
          'post_type' 		=> 'artigos',
          'orderby'           => 'date',
          'order' 		    => 'DESC',
          'posts_per_page'    => 6
  ));
  while ( have_posts() ) : the_post();
  $cont++; 
  $titulo = get_the_title();
  $link = get_post_meta( $post->ID, '_artigos_link', true );
  
  
   ?>
      <p class="artigo"><span class="numero-artigo"><?php echo $cont; ?>.</span><a class="link-artigo" href="<?php echo $link; ?>" target="_blank"><?php echo $titulo; ?></a></p>
  <?php
  if($cont == 3){
	  echo '</div>
	  <div class="col-md-6">';
  };
  
  
  wp_reset_postdata();
  
  endwhile; 
  
  ?>

			</div>
		</div>
	</div>
</section>
<section class="sugestoes-de-formulas">
	<div class="container">
    	<h3 class="titulo-cinza">SUGESTÕES DE FORMÚLAS</h3>
        <div class="barra-titulo-verde"></div>
          <div class="row">
           	<?php 
    $wp_query = new WP_Query(array( 
          'post_type' 		=> 'post',
          'orderby'           => 'date',
          'order' 		    => 'DESC',
          'posts_per_page'    => 6,
		  'category_name'		=> 'sugestoes-de-formulas'
  ));
  while ( have_posts() ) : the_post();
   ?>
          
           <div class="col-md-2">
           	<article class="box-sugestao">
           		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/icone-sugestoes.png" class="img-responsive icone-sugestao"></a>
	            <h2 class="titulo-sugestao"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			</article>
           </div>
  <?php
  wp_reset_postdata();
  
  endwhile; 
  
  ?>
           
        </div>
   </div>
</section>

<?php get_footer(); ?>