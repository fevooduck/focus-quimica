<?php get_header(); 

/* Template Name: Artigos */

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1><?php the_title(); ?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
	</div>
</section>
<?php endwhile; endif; ?>
<section class="internas">
	<div class="container">
        <div class="row margin-conteudo">
        	<div class="col-md-8 text-justify">
            		<?php $cont=0;
    $wp_query = new WP_Query(array( 
          'post_type' 		=> 'artigos',
          'orderby'           => 'menu_order',
          'order' 		    => 'ASC',
          'posts_per_page'    => 10
  ));
  while ( have_posts() ) : the_post();
  $cont++; 
  $titulo = get_the_title();
  $link = get_post_meta( $post->ID, '_artigos_link', true );
  
  
   ?>
      <p class="artigo"><span class="numero-artigo-interno"><?php echo $cont; ?>.</span><a class="link-artigo-interno" href="<?php echo $link; ?>" target="_blank"><?php echo $titulo; ?></a></p>
  <?php
  wp_reset_postdata();
  
  endwhile; 
  
  ?>

            </div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
