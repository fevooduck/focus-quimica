<?php get_header(); 

/* Template Name: Representada Mibelle*/

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); 

$categoria = get_post_meta($post->ID,'marca', true);
?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1><?php the_title(); ?></h1>
                   <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    <span class="seta-cab"></span>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas">
	<div class="container">
        <div class="row margin-conteudo">
        	<div class="col-md-8 text-justify">
            	<?php the_content('Read the rest of this entry &raquo;'); ?>
<?php endwhile; endif; ?>
           <div class="row row-produto">
   <?php 
			   $cont=0;
   $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => $post->ID, 'orderby'=> 'menu_order', 'order' => 'ASC', 'posts_per_page'    => -1));
    // run the loop based on the query
    if ( $query->have_posts() ) { 
	while ($query->have_posts()) : $query->the_post();
	$cont++;
		
	global $post;
	  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
	  $titulo = get_the_title();
      $link = get_the_permalink();
	?>
<div class="col-md-4">
	<div class="box-produto">
		<a href="<?php echo $link; ?>">
			<img class="img-responsive img-produtos" src="<?php echo $html[0]; ?>" alt="<?php echo $titulo; ?>">
		</a>
		<p><a href="<?php echo $link; ?>"><?php echo $titulo; ?></a></p>
	</div>
</div>
 <?php if($cont==3){
		$cont=0;
		echo '</div><div class="row row-produto">';
	} ?>
  <?php endwhile;
        //$myvariable = ob_get_clean();
        //return $myvariable;
        wp_reset_postdata(); }?>
				</div>
           
            </div>

        	<div class="col-md-4">
            	<div class="box-representada">
                   	   <?php 
					   $query = new WP_Query( array( 'post_type' => 'representadas', 'orderby'=> 'date', 'order' => 'ASC', 'meta_key' => '_representadas_marca', 'meta_value' => $categoria));
    // run the loop based on the query
    if ( $query->have_posts() ) { 
	while ($query->have_posts()) : $query->the_post();
	
	global $post;
	  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
	  $frase = get_post_meta( $post->ID, '_representadas_frase', true );
      $pais = get_post_meta( $post->ID, '_representadas_pais', true );;
      $site = get_post_meta( $post->ID, '_representadas_site', true );
	?>

            		<img src="<?php echo $html[0]; ?>" alt="<?php echo $titulo; ?>" class="img-responsive">
            		<h3 class="frase-representada-interna"><?php echo $frase; ?></h3>
					<p>País: <?php echo $pais; ?></p>
					<p>Site: <a href="http://<?php echo $site; ?>" alt="<?php the_title(); ?>" target="_blank"><?php echo $site; ?></a></p>
					  <?php endwhile;
        //$myvariable = ob_get_clean();
        //return $myvariable;
        wp_reset_postdata(); }?>
				</div>
            </div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
